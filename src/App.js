import React, { Component } from "react";
import Title from "./Components/Title";
import Form from "./Components/Form";
import TodoList from "./Components/TodoList";
import SimpleContext from "./ContextApi/ContextApi";

class App extends Component {
  state = {
    inputValue: "",
    Todo: [],
    condition: false,
  };

  addTodoToListHandler = (e) => {
    e.preventDefault();

    const fakeTodo = [
      ...this.state.Todo,
      {
        id: Math.random() * 1000,
        todo: this.state.inputValue,
        complete: false,
      },
    ];

    this.setState({
      Todo: fakeTodo,
      inputValue: "",
    });
  };
  checkLocalStorage = () => {
    localStorage.setItem("Todo", JSON.stringify(this.state.Todo));
  };

  componentWillMount() {
    localStorage.getItem("Todo") && this.setState({
      Todo : JSON.parse(localStorage.getItem("Todo"))
    })
  }

  AddInputValueToList = (e) => {
    this.setState({
      inputValue: e.target.value,
    });
  };

  removeTodoOfList = (id) => {
    const fakeTodo = [...this.state.Todo];
    let filter = fakeTodo.filter((item) => item.id !== id);

    this.setState({
      Todo: filter,
    });
  };

  setCompleted = (id) => {
    let fakeTodo = [...this.state.Todo];

    this.setState(
      fakeTodo.map((item) => {
        if (item.id === id) {
          item.complete = !item.complete;
        }
        return item.complete;
      })
    );
  };

  render() {
    return (
      <SimpleContext.Provider
        value={{
          state: this.state,
          addTodoToListHandler: this.addTodoToListHandler,
          AddInputValueToList: this.AddInputValueToList,
          removeTodoOfList: this.removeTodoOfList,
          setCompleted: this.setCompleted,
        }}
      >
        <div>
          <Title />
          <Form />
          <TodoList />
          {this.checkLocalStorage}
        </div>
        {this.checkLocalStorage()}
      </SimpleContext.Provider>
    );
  }
}

export default App;
