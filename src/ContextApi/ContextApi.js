import { createContext } from "react";

const SimpleContext = createContext({
  state: {},
  addTodoToListHandler: () => {},
  AddInputValueToList : () => {} ,
  removeTodoOfList : () => {} ,
  setCompleted : () => {} ,
});

export default SimpleContext
