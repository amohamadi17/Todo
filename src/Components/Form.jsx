import React, { useContext } from "react";
import SimpleContext from "./../ContextApi/ContextApi";

const Form = () => {
  const context = useContext(SimpleContext);

  return (
    <form className="form-inline justify-content-center">
      <input
        type="text"
        placeholder="Add Todo"
        onChange={context.AddInputValueToList}
        value={context.state.inputValue}
      />
      <div className="input-group-prepend">
        <button
          className="btn btn-sm  input-group-text btn-primary bg-primary"
          type="submit"
          onClick={context.addTodoToListHandler}
        >
          <i className="fa fa-plus text-light"></i>
        </button>
      </div>
    </form>
  );
};

export default Form;
