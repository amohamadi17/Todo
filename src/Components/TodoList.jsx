import React, { useContext } from "react";
import SimpleContext from "./../ContextApi/ContextApi";
import Todo from "./Todo";

const TodoList = () => {
  const context = useContext(SimpleContext);

  return (
    <div className="d-flex justify-content-center">
      <div className="col-8 col-md-8 col-sm-8 ">
        {context.state.Todo.map((item) => (
          <Todo key={item.id} item={item} />
        ))}
      </div>
    </div>
  );
};

export default TodoList;
