import React, { useContext, useEffect } from "react";
import SimpleContext from "./../ContextApi/ContextApi";

import "./Todo.css";

const Todo = ({ item }) => {
  const context = useContext(SimpleContext);

  return (
    <ul className="d-flex justify-content-center mt-4">
      <li
        style={{ listStyle: "none" }}
        className="d-flex justify-content-between w-100 mt-3"
      >
        <p className={`mt-2 ${item.complete ? "completed" : ""}`}>{item.todo}</p>
        <div>
          <button
            className="btn btn-sm btn-success m-1"
            onClick={() => context.setCompleted(item.id)}
          >
            <i className="fa fa-check mt-1"></i>
          </button>
          <button
            className="btn btn-sm btn-danger m-1"
            onClick={() => context.removeTodoOfList(item.id)}
          >
            <i className="fa fa-trash mt-1"></i>
          </button>
        </div>
      </li>
    </ul>
  );
};

export default Todo;
