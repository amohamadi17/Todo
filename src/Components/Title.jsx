import React from "react";

const Title = () => {
  return (
    <h2 style={{ textAlign: "center" }} className="p-3 mt-3">
      Todo App
    </h2>
  );
};

export default Title;
